module.exports = {
  semi: false,
  singleQuote: true,
  trailingComma: 'all',
  overrides: [
    {
      files: '*.svelte',
      plugins: ['svelte'],
    },
  ],
}
