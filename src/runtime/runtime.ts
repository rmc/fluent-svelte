import { FluentContext } from './context'
import {
  FluentArgument,
  FluentType,
  FluentNumber,
  FluentDateTime,
  FluentNone,
  FluentValue,
} from './types'

export function resolveVariableReference(
  context: FluentContext,
  name: string,
  arg: FluentArgument,
): FluentValue {
  if (arg instanceof FluentType) {
    return arg.setContext(context)
  }

  switch (typeof arg) {
    case 'string':
      return arg
    case 'number':
      return new FluentNumber(arg).setContext(context)
    case 'object':
      if (arg instanceof Date) {
        return new FluentDateTime(arg.getTime()).setContext(context)
      }
    default:
      context.reportError(
        TypeError(`Variable type not supported: $${name}, ${typeof arg}`),
      )
      return new FluentNone(`$${name}`).setContext(context)
  }
}

export function callFunctionReference(
  context: FluentContext,
  name: string,
  positional: Array<FluentValue>,
  named: Record<string, FluentValue>,
): FluentValue {
  let func = context.getFunction(name)
  if (!func) {
    context.reportError(new ReferenceError(`Unknown function: ${name}()`))
    return new FluentNone(`${name}()`).setContext(context)
  }

  if (typeof func !== 'function') {
    context.reportError(new TypeError(`Function ${name}() is not callable`))
    return new FluentNone(`${name}()`).setContext(context)
  }

  try {
    const result = func(positional, named)
    if (result instanceof FluentType) {
      result.setContext(context)
    }
    return result
  } catch (err) {
    context.reportError(err)
    return new FluentNone(`${name}()`).setContext(context)
  }
}

export function match(
  context: FluentContext,
  selector: FluentValue,
  key: FluentValue,
): boolean {
  if (key === selector) {
    return true
  }
  if (
    key instanceof FluentNumber &&
    selector instanceof FluentNumber &&
    key.value === selector.value
  ) {
    return true
  }
  if (selector instanceof FluentNumber && typeof key === 'string') {
    let category = context
      .memoizeIntlObject(
        Intl.PluralRules,
        selector.opts as Intl.PluralRulesOptions,
      )
      .select(selector.value)
    if (key === category) {
      return true
    }
  }

  return false
}
