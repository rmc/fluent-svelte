import {
  FluentValue,
  FluentNone,
  FluentNumber,
  FluentDateTime,
} from './types.js'

export function NUMBER(
  args: Array<FluentValue>,
  opts: Record<string, FluentValue>,
): FluentValue {
  let arg = args[0]

  if (arg instanceof FluentNone) {
    return new FluentNone(`NUMBER(${arg.valueOf()})`)
  }

  if (arg instanceof FluentNumber || arg instanceof FluentDateTime) {
    return new FluentNumber(arg.valueOf(), {
      ...arg.opts,
      ...opts,
    })
  }

  throw new TypeError('Invalid argument to NUMBER')
}

export function DATETIME(
  args: Array<FluentValue>,
  opts: Record<string, FluentValue>,
): FluentValue {
  let arg = args[0]

  if (arg instanceof FluentNone) {
    return new FluentNone(`DATETIME(${arg.valueOf()})`)
  }

  if (arg instanceof FluentNumber || arg instanceof FluentDateTime) {
    return new FluentDateTime(arg.valueOf(), {
      ...arg.opts,
      ...opts,
    })
  }

  throw new TypeError('Invalid argument to DATETIME')
}
