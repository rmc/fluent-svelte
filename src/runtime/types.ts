import { FluentContext } from './context'

export type FluentValue = FluentType<unknown> | string

export type FluentArgument = FluentType<unknown> | string | number | Date

export type FluentFunction = (
  positional: Array<FluentValue>,
  named: Record<string, FluentValue>,
  context?: FluentContext,
) => FluentValue

export class FluentType<T> {
  public value: T
  public context: FluentContext | undefined

  constructor(value: T) {
    this.value = value
  }

  setContext(context: FluentContext) {
    this.context = context
    return this
  }

  valueOf(): T {
    return this.value
  }

  toString(): string {
    throw new Error('Subclasses of FluentType must implement toString.')
  }
}

export class FluentNone extends FluentType<string> {
  constructor(value = '???') {
    super(value)
  }

  toString(): string {
    return `{${this.value}}`
  }
}

export class FluentNumber extends FluentType<number> {
  public opts: Intl.NumberFormatOptions

  constructor(value: number, opts: Intl.NumberFormatOptions = {}) {
    super(value)
    this.opts = opts
  }

  toString(): string {
    if (!this.context) {
      throw new Error('fluent-svelte: Missing context')
    }
    try {
      const nf = this.context.memoizeIntlObject(Intl.NumberFormat, this.opts)
      return nf.format(this.value)
    } catch (err) {
      this.context.reportError(err)
      return this.value.toString(10)
    }
  }
}

export class FluentDateTime extends FluentType<number> {
  public opts: Intl.DateTimeFormatOptions

  constructor(value: number, opts: Intl.DateTimeFormatOptions = {}) {
    super(value)
    this.opts = opts
  }

  toString(): string {
    if (!this.context) {
      throw new Error('fluent-svelte: Missing context')
    }
    try {
      const dtf = this.context.memoizeIntlObject(Intl.DateTimeFormat, this.opts)
      return dtf.format(this.value)
    } catch (err) {
      this.context.reportError(err)
      return new Date(this.value).toISOString()
    }
  }
}
