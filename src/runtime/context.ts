import * as builtins from './functions'
import { FluentFunction } from './types'

export class FluentContext {
  public locales: string[]
  private _functions: Record<string, FluentFunction>
  private _intls: WeakMap<object, Record<string, object>>
  private _onError?: (error: Error) => void

  constructor(
    locales: string[],
    {
      functions,
      onError,
    }: {
      functions?: Record<string, FluentFunction>
      onError?: (error: Error) => void
    } = {},
  ) {
    this.locales = Array.isArray(locales) ? locales : [locales]
    this._functions = {
      ...builtins,
      ...functions,
    }
    this._intls = new WeakMap()
    this._onError = onError
  }

  getFunction(name: string): FluentFunction | undefined {
    return this._functions[name]
  }

  reportError(error: Error): void {
    if (!this._onError) {
      throw error
    }
    this._onError(error)
  }

  memoizeIntlObject<ObjectT extends object, OptionsT>(
    ctor: new (locales: Array<string>, opts: OptionsT) => ObjectT,
    opts: OptionsT,
  ): ObjectT {
    let cache = this._intls.get(ctor)
    if (!cache) {
      cache = {}
      this._intls.set(ctor, cache)
    }
    let id = JSON.stringify(opts)
    if (!cache[id]) {
      cache[id] = new ctor(this.locales, opts)
    }
    return cache[id] as ObjectT
  }
}

export const contextKey = {}
