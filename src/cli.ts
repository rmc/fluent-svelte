import { readFileSync, existsSync, mkdirSync, writeFileSync } from 'fs'
import { resolve, join } from 'path'
import { FluentResource } from '@fluent/bundle'
import { Compiler, print } from 'fluent-svelte/compiler'

const [, , destination, ...messages] = process.argv

messages.forEach(messages => {
  const locale = getLocale(messages)

  const contents = readFileSync(resolve(process.cwd(), messages))

  const resource = new FluentResource(contents.toString())
  const compiler = new Compiler()
  const errors = compiler.addResource(resource)

  if (errors.length) {
    errors.forEach(error => {
      console.error(error)
    })
  }

  compiler.messages.forEach(message => {
    const destinationDir = join(process.cwd(), destination)
    const destinationLocaleDir = join(destinationDir, locale)
    const destinationFilePath = join(
      destinationLocaleDir,
      `${message.id}.svelte`,
    )
    if (!existsSync(destinationDir)) {
      mkdirSync(destinationDir)
    }
    if (!existsSync(destinationLocaleDir)) {
      mkdirSync(destinationLocaleDir)
    }
    try {
      writeFileSync(
        destinationFilePath,
        print(compiler.compileMessageToSvelte(message)),
      )
    } catch (error) {
      throw new Error(
        `fluent-svelte cli: failed to compile ${message.id} for locale: ${locale}`,
      )
    }
  })
})

function getLocale(value: string): string {
  const result = value.match(/\.([\w-_]+)\.ftl/)
  if (!result || result.length < 2) {
    throw new Error(
      `fluent-svelte cli: failed to infer locale from message file ${value}`,
    )
  }
  return result[1]
}
