import prettier, { BuiltInParserName } from 'prettier'
import * as prettierPluginSvelte from 'prettier-plugin-svelte'
import { print as recastPrint } from 'recast'
import { Node } from 'estree'
import {
  Ast,
  TemplateNode,
  MustacheTag,
  Slot,
  IfBlock,
  Attribute,
  Script,
  EachBlock,
  AwaitBlock,
  EventHandler,
  Binding,
  Let,
  Class,
  Transition,
  Action,
  Animation,
  RawMustacheTag,
  Spread,
  InlineComponent,
  Element,
  Window,
  Head,
  Title,
  Style,
} from './types'

declare module 'prettier' {
  interface Ast {
    tokens: any[]
    html: TemplateNode
    module?: Script | undefined
    instance?: Script | undefined
    css?: Style | undefined
  }

  interface ExtendedOptions extends Options {
    originalText: string
  }

  interface debug {
    formatAST(
      ast: Ast,
      options: ExtendedOptions,
    ): {
      formatted: string
    }
  }

  const __debug: debug
}

class Printer {
  index: number
  originalText: string

  constructor(ast: Ast) {
    this.index = 0
    this.originalText = ''

    if (ast.module) {
      this.printScript(ast.module, true)
    }
    if (ast.instance && ast.instance.content.body.length) {
      this.printScript(ast.instance)
    }
    if (ast.css) {
      this.printStyle(ast.css)
    }
    if (ast.html.children) {
      this.printTree(ast.html.children)
    }
  }

  printStyle(style: Style) {
    const {
      content: { styles },
    } = style
    const encoded = Buffer.from(styles).toString('base64')
    style.attributes.push({
      type: 'Attribute',
      name: '✂prettier:content✂',
      value: [
        {
          type: 'Text',
          data: encoded,
        },
      ],
    })
  }

  printScript(script: Script, isModule = false) {
    const initialIndex = this.index
    this.printTree(script.content.body)
    const encoded = Buffer.from(this.originalText.slice(initialIndex)).toString(
      'base64',
    )
    const scriptOpeningTag = `<script ✂prettier:content✂="${encoded}"${
      isModule ? ' context="module"' : ''
    }>`
    this.originalText = this.originalText.slice(0, initialIndex)
    this.index = initialIndex
    script.start = this.index
    this.originalText += scriptOpeningTag
    this.index += scriptOpeningTag.length
    script.content.start = this.index
    const scriptContent = '{}'
    this.originalText += scriptContent
    script.content.body = [
      {
        type: 'BlockStatement',
        body: [],
      },
    ]
    this.index += scriptContent.length
    script.content.end = this.index
    const scriptClosingTag = '</script>'
    this.originalText += scriptClosingTag
    this.index += scriptClosingTag.length
    script.end = this.index
  }

  printTree(nodes: (TemplateNode | Node)[]) {
    nodes.forEach(node => {
      switch (node.type) {
        case 'Text':
        case 'Comment':
        case 'AttributeShorthand':
          return
        case 'Fragment':
          if (node.children) return this.printTree(node.children)
          return
        case 'Slot':
        case 'Element':
        case 'Window':
        case 'Head':
        case 'Title':
          const el = node as Slot | Element | Window | Head | Title
          if (el.attributes) this.printTree(el.attributes)
          if (el.children) this.printTree(el.children)
          return
        case 'InlineComponent':
          const inlineComponent = node as InlineComponent
          if (inlineComponent.attributes)
            this.printTree(inlineComponent.attributes)
          if (inlineComponent.children) this.printTree(inlineComponent.children)
          if (inlineComponent.expression)
            this.printNode(inlineComponent.expression)
          return

        case 'IfBlock':
          const ifBlock = node as IfBlock
          this.printNode(ifBlock.expression)
          this.printTree(ifBlock.children)
          if (ifBlock.else && ifBlock.else.children)
            this.printTree(ifBlock.else.children)
          return
        case 'EachBlock':
          const eachBlock = node as EachBlock
          this.printNode(eachBlock.expression)
          this.printNode(eachBlock.context)
          if (eachBlock.index) this.printNode(eachBlock.index)
          if (eachBlock.key) this.printNode(eachBlock.key)
          if (eachBlock.children) this.printTree(eachBlock.children)
          return
        case 'AwaitBlock':
          const awaitBlock = node as AwaitBlock
          this.printNode(awaitBlock.expression)
          this.printTree(awaitBlock.pending.children)
          if (awaitBlock.then) this.printTree(awaitBlock.then.children)
          if (awaitBlock.catch) this.printTree(awaitBlock.catch.children)
          return
        case 'Attribute':
          return this.printTree((node as Attribute).value)
        case 'EventHandler':
        case 'Binding':
        case 'Class':
        case 'Let':
        case 'MustacheTag':
        case 'RawMustacheTag':
        case 'Spread':
          return this.printNode(
            (node as
              | EventHandler
              | Binding
              | Class
              | Let
              | MustacheTag
              | RawMustacheTag
              | Spread).expression,
          )
        case 'Transition':
        case 'Action':
        case 'Animation':
          const n = node as Transition | Action | Animation
          if (n.expression) return this.printNode(n.expression)
          return
        default:
          return this.printNode(node)
      }
    })
  }

  printNode(node: any) {
    try {
      const text = recastPrint(node).code
      if (text.length) {
        node.start = this.index
        this.originalText += text
        this.index += text.length
        node.end = this.index
      }
    } catch (error) {
      throw new Error(`fluent-svelte Printer: unknown node ${node.type}`)
    }
  }
}

export function print(ast: Ast): string {
  const prettierAst = { ...ast, tokens: [] }
  const printer = new Printer(prettierAst)
  return prettier.__debug.formatAST(prettierAst, {
    originalText: printer.originalText,
    parser: 'svelte' as BuiltInParserName,
    plugins: [prettierPluginSvelte],
  }).formatted
}
