import {
  ArrayExpression,
  AssignmentExpression,
  AssignmentOperator,
  AssignmentProperty,
  BlockStatement,
  CallExpression,
  Comment,
  Declaration,
  Directive,
  ExportDefaultDeclaration,
  ExportNamedDeclaration,
  ExportSpecifier,
  Expression,
  ExpressionStatement,
  FunctionDeclaration,
  Identifier,
  ImportDeclaration,
  ImportDefaultSpecifier,
  ImportSpecifier,
  LabeledStatement,
  Literal,
  MemberExpression,
  ModuleDeclaration,
  NewExpression,
  ObjectExpression,
  ObjectPattern,
  Pattern,
  Property,
  Statement,
  VariableDeclaration,
  VariableDeclarator,
  BinaryExpression,
  BinaryOperator,
  ConditionalExpression,
} from 'estree'

interface BaseNode {
  type: string
  children?: TemplateNode[]
  start?: number
  end?: number
}
export interface Fragment extends BaseNode {
  type: 'Fragment'
  children: TemplateNode[]
}
export interface Text extends BaseNode {
  type: 'Text'
  data: string
}
export interface MustacheTag extends BaseNode {
  type: 'MustacheTag'
  expression: Expression
}

export interface RawMustacheTag extends BaseNode {
  type: 'RawMustacheTag'
  expression: Expression
}

export interface Spread extends BaseNode {
  type: 'Spread'
  expression: Expression
}
export interface IfBlock extends BaseNode {
  type: 'IfBlock'
  elseif?: Boolean
  expression: Expression
  else?: ElseBlock
  children: TemplateNode[]
}
export interface ElseBlock extends BaseNode {
  type: 'ElseBlock'
  children: TemplateNode[]
}

export interface InlineComponent extends BaseNode {
  type: 'InlineComponent'
  name: string
  attributes?: Attr[]
  expression?: Expression
}

export interface Slot extends BaseNode {
  type: 'Slot'
  name: string
  children: TemplateNode[]
  attributes?: Attr[]
}

export interface Element extends BaseNode {
  type: 'Element'
  name: string
  children: TemplateNode[]
  attributes?: (Attr | Class)[]
}

export interface Window extends BaseNode {
  type: 'Window'
  name: string
  children: TemplateNode[]
  attributes?: Attr[]
}
export interface Head extends BaseNode {
  type: 'Head'
  name: string
  children: TemplateNode[]
  attributes?: Attr[]
}

export interface Title extends BaseNode {
  type: 'Head'
  name: string
  children: TemplateNode[]
  attributes?: Attr[]
}

export interface Attribute extends BaseNode {
  type: 'Attribute'
  name: string
  value: TemplateNode[]
}

export interface EventHandler extends BaseNode {
  type: 'EventHandler'
  name: string
  expression?: Expression
  modifiers?: string[]
}

export interface Binding extends BaseNode {
  type: 'Binding'
  name: string
  expression: Expression
}

export interface Class extends BaseNode {
  type: 'Class'
  name: string
  expression: Expression
}

export interface Let extends BaseNode {
  type: 'Let'
  name: string
  modifiers?: string[]
  expression?: Expression
}

export interface Transition extends BaseNode {
  type: 'Transition'
  name: string
  expression?: Expression
  modifiers?: string[]
  intro: boolean
  outro: boolean
}

export interface Action extends BaseNode {
  type: 'Action'
  name: string
  expression?: Expression
}

export interface Animation extends BaseNode {
  type: 'Animation'
  name: string
  expression?: Expression
}

export interface EachBlock extends BaseNode {
  type: 'EachBlock'
  expression: Expression
  context: Pattern
  key?: Expression
  index?: Identifier
  else?: ElseBlock
}
export interface ThenBlock extends BaseNode {
  type: 'ThenBlock'
  children: TemplateNode[]
}

export interface PendingBlock extends BaseNode {
  type: 'PendingBlock'
  children: TemplateNode[]
}

export interface CatchBlock extends BaseNode {
  type: 'CatchBlock'
  children: TemplateNode[]
}
export interface AwaitBlock extends BaseNode {
  type: 'AwaitBlock'
  expression: Expression
  value?: string
  error?: string
  pending: PendingBlock
  then?: ThenBlock
  catch?: CatchBlock
}

export declare type Attr = Attribute | Let | Spread | EventHandler

export declare type TemplateNode =
  | BaseNode
  | Text
  | MustacheTag
  | IfBlock
  | ElseBlock
  | InlineComponent
  | Slot

export interface Program extends BaseNode {
  type: 'Program'
  sourceType: 'script' | 'module'
  body: Array<Directive | Statement | ModuleDeclaration>
  comments?: Array<Comment>
}
export interface Script extends BaseNode {
  type: 'Script'
  context: string
  content: Program
}

export interface Style extends BaseNode {
  type: 'Style'
  attributes: any[]
  children: any[]
  content: {
    start: number
    end: number
    styles: string
  }
}

export interface Ast {
  html: TemplateNode
  module?: Script
  instance?: Script
  css?: Style
}

export type InstanceNode =
  | ImportDeclaration
  | ExportNamedDeclaration
  | LabeledStatement
  | VariableDeclaration

export function makeAst(
  html: TemplateNode[] = [],
  instance?: InstanceNode[],
): Ast {
  const ast: Ast = {
    html: {
      type: 'Fragment',
      children: html,
    },
  }
  if (instance) {
    ast.instance = makeInstance(instance)
  }
  return ast
}

export function makeMustacheTag(expression: Expression): MustacheTag {
  return {
    type: 'MustacheTag',
    expression,
  }
}

export function makeText(value: string): Text {
  return { type: 'Text', data: value }
}

export function makeAttribute(name: string, value: TemplateNode[]): Attribute {
  return {
    type: 'Attribute',
    name,
    value,
  }
}

export function makeLet(
  name: string,
  expression?: Expression,
  modifiers?: string[],
): Let {
  return {
    type: 'Let',
    name,
    expression,
    modifiers,
  }
}

export function makeIfBlock(
  expression: Expression,
  children: TemplateNode[] = [],
  elseif: Boolean = false,
  _else?: ElseBlock,
): IfBlock {
  return {
    type: 'IfBlock',
    else: _else,
    elseif,
    expression,
    children,
  }
}

export function makeElseBlock(children: TemplateNode[] = []): ElseBlock {
  return {
    type: 'ElseBlock',
    children,
  }
}

export function makeInlineComponent(
  name: string,
  attributes?: Attr[],
  children?: TemplateNode[],
): InlineComponent {
  return {
    type: 'InlineComponent',
    name,
    attributes,
    children: children || [],
  }
}

export function makeSlot(attributes?: Attr[], children?: TemplateNode[]): Slot {
  return {
    type: 'Slot',
    name: 'slot',
    attributes,
    children: children || [],
  }
}

export function makeIdentifier(name: string): Identifier {
  return {
    type: 'Identifier',
    name,
  }
}

export function makeStringLiteral(value: string): Literal {
  return {
    type: 'Literal',
    value,
    raw: `'${value.replace("'", "\\'")}'`,
  }
}
export function makeNumberLiteral(value: number): Literal {
  return {
    type: 'Literal',
    value: value.toString(),
    raw: value.toString(),
  }
}

export function makeImportDefaultSpecifier(
  local: Identifier,
): ImportDefaultSpecifier {
  return {
    type: 'ImportDefaultSpecifier',
    local,
  }
}

export function makeImportSpecifier(
  imported: Identifier,
  local: Identifier = imported,
): ImportSpecifier {
  return {
    type: 'ImportSpecifier',
    imported,
    local,
  }
}

export function makeCallExpression(
  callee: Expression,
  _arguments: Expression[] = [],
): CallExpression {
  return {
    type: 'CallExpression',
    callee,
    ['arguments']: _arguments,
  }
}

export function makeNewExpression(
  callee: Expression,
  _arguments: Expression[] = [],
): NewExpression {
  return {
    type: 'NewExpression',
    callee,
    ['arguments']: _arguments,
  }
}

export function makeConditionalExpression(
  test: Expression,
  consequent: Expression,
  alternate: Expression,
): ConditionalExpression {
  return {
    type: 'ConditionalExpression',
    test,
    consequent,
    alternate,
  }
}

export function makeArrayExpression(
  elements: Expression[] = [],
): ArrayExpression {
  return {
    type: 'ArrayExpression',
    elements,
  }
}

export function makeProperty(
  key: Expression,
  value: Expression | Pattern,
  shorthand: boolean = false,
): Property {
  return {
    type: 'Property',
    key,
    value,
    shorthand,
    kind: 'init',
    method: false,
    computed: false,
  }
}

export function makeAssignmentProperty(
  key: Expression,
  value: Pattern,
): AssignmentProperty {
  return {
    type: 'Property',
    key,
    value,
    shorthand: true,
    kind: 'init',
    method: false,
    computed: false,
  }
}

export function makeObjectExpression(
  properties: Property[] = [],
): ObjectExpression {
  return {
    type: 'ObjectExpression',
    properties: properties,
  }
}

export function makeObjectPattern(
  properties: AssignmentProperty[] = [],
): ObjectPattern {
  return {
    type: 'ObjectPattern',
    properties: properties,
  }
}

export function makeLabeledStatement(
  label: Identifier,
  body: Statement,
): LabeledStatement {
  return {
    type: 'LabeledStatement',
    label,
    body,
  }
}

export function makeExpressionStatement(
  expression: Expression,
): ExpressionStatement {
  return {
    type: 'ExpressionStatement',
    expression,
  }
}

export function makeAssignmentExpression(
  operator: AssignmentOperator,
  left: Pattern | MemberExpression,
  right: Expression,
): AssignmentExpression {
  return {
    type: 'AssignmentExpression',
    operator,
    left,
    right,
  }
}

export function makeExportNamedDeclaration(
  declaration: Declaration,
  specifiers: ExportSpecifier[] = [],
): ExportNamedDeclaration {
  return {
    type: 'ExportNamedDeclaration',
    declaration,
    specifiers,
  }
}

export function makeExportDefaultDeclaration(
  declaration: Declaration,
): ExportDefaultDeclaration {
  return {
    type: 'ExportDefaultDeclaration',
    declaration,
  }
}

export function makeVariableDeclaration(
  declarations: VariableDeclarator[],
  kind: 'var' | 'let' | 'const',
): VariableDeclaration {
  return {
    type: 'VariableDeclaration',
    declarations,
    kind,
  }
}

export function makeVariableDeclarator(
  id: Pattern,
  init?: Expression,
): VariableDeclarator {
  return {
    type: 'VariableDeclarator',
    id,
    init,
  }
}

export function makeMemberExpression(
  object: Expression,
  property: Expression,
  computed: boolean,
): MemberExpression {
  return {
    type: 'MemberExpression',
    object,
    property,
    computed,
  }
}

export function makeBinaryExpression(
  operator: BinaryOperator,
  left: Expression,
  right: Expression,
): BinaryExpression {
  return { type: 'BinaryExpression', operator, left, right }
}

export function makeBlockStatement(body: Statement[]): BlockStatement {
  return {
    type: 'BlockStatement',
    body,
  }
}

export function makeFunctionDeclaration(
  id: Identifier,
  params: Pattern[],
  body: BlockStatement,
): FunctionDeclaration {
  return {
    id,
    type: 'FunctionDeclaration',
    params,
    body,
  }
}

export function makeProgram(
  body: (Directive | Statement | ModuleDeclaration)[],
  sourceType: 'script' | 'module',
): Program {
  return {
    type: 'Program',
    sourceType,
    body,
  }
}

export function makeScript(content: Program, context: string): Script {
  return {
    type: 'Script',
    context,
    content,
  }
}

/* helpers */

export function makeImportDeclaration(
  moduleName: string,
  namedImports: string[],
  defaultImport?: string,
): ImportDeclaration {
  const specifiers = []
  if (defaultImport) {
    specifiers.push(makeImportDefaultSpecifier(makeIdentifier(defaultImport)))
  }
  namedImports.forEach(namedImport => {
    specifiers.push(makeImportSpecifier(makeIdentifier(namedImport)))
  })
  return {
    type: 'ImportDeclaration',
    source: makeStringLiteral(moduleName),
    specifiers,
  }
}

export function makeInstance(body: InstanceNode[]): Script {
  return makeScript(makeProgram(body, 'module'), 'default')
}

export function makePropDeclaration(name: Identifier) {
  return makeExportNamedDeclaration(
    makeVariableDeclaration([makeVariableDeclarator(name)], 'let'),
  )
}

export function makeReactiveExpression(
  name: Identifier,
  expression: Expression,
) {
  return makeLabeledStatement(
    makeIdentifier('$'),
    makeExpressionStatement(makeAssignmentExpression('=', name, expression)),
  )
}
