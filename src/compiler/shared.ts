export function buildComponentName(messageId: string, locale: string): string {
  return `${messageId
    .split('-')
    .map(capitalize)
    .join('')}$${locale.replace(/[-_]/, '')}`
}

export function camelCase(value: string): string {
  const parts = value.split(/-/)
  return [parts[0], ...parts.slice(1).map(capitalize)].join('')
}

export function capitalize(value: string): string {
  return value[0].toUpperCase() + value.slice(1)
}
