import { parse, walk } from 'svelte/compiler'
import { Processed, PreprocessorGroup } from 'svelte/types/compiler/preprocess'
import { print } from './print'
import { join, resolve } from 'path'
import { readdirSync, statSync } from 'fs'
import { ImportDeclaration } from 'estree'
import { buildComponentName } from './shared'
import {
  Ast,
  TemplateNode,
  InlineComponent,
  InstanceNode,
  makeInstance,
  makeInlineComponent,
  makeLet,
  makeIdentifier,
  Text,
  makeAttribute,
  makeMemberExpression,
  makeObjectExpression,
  makeProperty,
  makeStringLiteral,
  makeNumberLiteral,
  makeImportDeclaration,
  makeMustacheTag,
} from './types'

declare module 'svelte/compiler' {
  export function walk(ast: Ast, options: any): void
}
export type Options = {
  dir: string
  locales?: string[]
  componentName?: string
}
type Context = {
  dir: string
  locales: string[]
  componentName: string
}
type Components = {
  [locale: string]: {
    name: string
    path: string
  }
}
type Dependencies = {
  [messageId: string]: Components
}
type astPath = Set<TemplateNode | Ast>

const DEFAULT_COMPONENT_NAME = 'Localized'
const LOCALIZATION_CONTEXT_MODULE = 'fluent-svelte/Context.svelte'
const LOCALIZATION_CONTEXT_COMPONENT_NAME = 'LocalizationContext'
const LOCALIZATION_CONTEXT_BINDING = 'localizationContext$'

export function preprocessor(options: Options): PreprocessorGroup {
  const context = getContext(
    options.dir,
    options.locales,
    options.componentName,
  )
  return {
    markup({ content, filename }): Processed {
      let preprocess = false
      const dependencies: Dependencies = {}
      const ast = parse(content)
      const pathsToLocalized: astPath[] = [] // Keep track of AST nodes leading to Localized instances
      let currentPathToLocalized: astPath = new Set()
      walk(ast, {
        enter(
          node: InstanceNode | TemplateNode,
          parent: InstanceNode | TemplateNode,
        ) {
          if (parent && !shouldSkip(node)) {
            currentPathToLocalized.add(parent)
          }
          if (node.type === 'InlineComponent') {
            const component = node as InlineComponent
            const messageId = getMessageId.call(context, component, filename)
            if (messageId) {
              preprocess = true
              const components: Components = {}
              context.locales.forEach(locale => {
                components[locale] = {
                  name: buildComponentName(messageId, locale),
                  path: resolve(context.dir, locale, `${messageId}.svelte`),
                }
              })
              dependencies[messageId] = components

              const newNode = processLocalized.call(
                context,
                component,
                components,
              )
              this.replace(newNode)
              currentPathToLocalized.add(newNode)
              pathsToLocalized.push(currentPathToLocalized)
              if (shouldSkip(component)) {
                currentPathToLocalized = new Set()
              }
            }
          }
          if (shouldSkip(node)) {
            this.skip()
          }
        },
      })

      if (preprocess) {
        const processedAst = ast as Ast

        wrapWithContext.call(context, pathsToLocalized)

        addImportDeclarations.call(context, dependencies, processedAst)

        return { code: print(processedAst) }
      }

      return { code: content }
    },
  }
}

function shouldSkip(node: Ast | InstanceNode | TemplateNode) {
  return !('html' in node || 'children' in node || 'else' in node)
}

function getContext(
  dir: string,
  locales: string[] = getLocales(dir),
  componentName: string = DEFAULT_COMPONENT_NAME,
): Context {
  return {
    dir,
    locales,
    componentName,
  }
}

// Compile `<Localized id="messageId" {...messageArgs}></Localized>` to `<svelte:component this={MessageComponent} {...messageArgs}></svelte:component>`
function processLocalized(
  this: Context,
  component: InlineComponent,
  components: Components,
): InlineComponent {
  return {
    ...component,
    name: 'svelte:component',
    attributes: [
      ...(component.attributes || []).filter(
        attr => 'name' in attr && attr.name !== 'id',
      ),
      makeAttribute('context', [
        makeMustacheTag(makeIdentifier(LOCALIZATION_CONTEXT_BINDING)),
      ]),
    ],
    expression: makeMemberExpression(
      makeObjectExpression(
        Object.entries(components).map(([locale, { name }]) =>
          makeProperty(makeStringLiteral(locale), makeIdentifier(name)),
        ),
      ),
      makeMemberExpression(
        makeMemberExpression(
          makeIdentifier(LOCALIZATION_CONTEXT_BINDING),
          makeIdentifier('locales'),
          false,
        ),
        makeNumberLiteral(0),
        true,
      ),
      true,
    ),
  }
}

function addImportDeclarations(
  this: Context,
  dependencies: Dependencies,
  ast: Ast,
) {
  const importDeclarations: ImportDeclaration[] = makeImportDeclarations.call(
    this,
    dependencies,
  )
  if (ast.instance) {
    ast.instance.content.body = [
      ...importDeclarations,
      ...ast.instance.content.body,
    ]
  } else {
    ast.instance = makeInstance(importDeclarations)
  }
}

// Wrap all Localized instances with <LocalizationContext></LocalizationContext>
function wrapWithContext(paths: astPath[]): void {
  // Find the top-most node containing all Localized instances
  const longestPath: astPath = getLongestPath(paths)
  const commonParent: TemplateNode | Ast | undefined = Array.from(
    longestPath.values(),
  ).find(pathNode => paths.every(path => path.has(pathNode)))

  // Find the index of the first and last child of the common parent containing Localized instances
  const commonParentChildren: TemplateNode[] =
    (commonParent &&
      (('html' in commonParent && commonParent.html.children) ||
        ('children' in commonParent && commonParent.children))) ||
    []
  const [firstChildIndex, lastChildIndex] = getChildIndexes(
    commonParentChildren,
    paths,
  )

  // Wrap children inside the range with context component
  const newChildren = [
    ...commonParentChildren.slice(0, firstChildIndex),
    makeInlineComponent(
      LOCALIZATION_CONTEXT_COMPONENT_NAME,
      [makeLet('context', makeIdentifier(LOCALIZATION_CONTEXT_BINDING))],
      commonParentChildren.slice(firstChildIndex, lastChildIndex! + 1),
    ),
    ...commonParentChildren.slice(lastChildIndex! + 1),
  ]
  if (commonParent && 'html' in commonParent) {
    commonParent.html.children = newChildren
  } else if (commonParent && 'children' in commonParent) {
    commonParent.children = newChildren
  }
}

function getLongestPath(
  paths: Set<Ast | TemplateNode>[],
): Set<Ast | TemplateNode> {
  let longest = paths[0]
  for (const path of paths) {
    if (path.size > longest.size) {
      longest = path
    }
  }
  return longest
}

function getChildIndexes(
  children: TemplateNode[],
  paths: astPath[],
): [number, number] {
  let firstChildIndex: number | undefined
  let lastChildIndex: number | undefined
  for (let i = 0; i < children.length; i++) {
    const child = children[i]
    if (paths.some(path => path.has(child))) {
      if (firstChildIndex) {
        lastChildIndex = i
      } else {
        firstChildIndex = i
      }
    }
  }
  // Single child
  lastChildIndex = lastChildIndex || firstChildIndex
  return [firstChildIndex!, lastChildIndex!]
}

function getMessageId(
  this: Context,
  component: InlineComponent,
  filename: string,
): string {
  let messageId = ''
  if (component.name === this.componentName) {
    const messageIdAttr = component?.attributes?.find(
      attr => 'name' in attr && attr.name === 'id',
    )
    if (messageIdAttr === undefined) {
      throw new Error(
        `fluent-svelte preprocess: Found <Localized/> but \`id\` attribute is missing in ${filename}`,
      )
    }
    messageId =
      (('value' in messageIdAttr &&
        messageIdAttr.value.length &&
        messageIdAttr.value[0]) as Text).data || ''
    if (messageId === '') {
      throw new Error(
        `fluent-svelte preprocess: Found malformed id attribute on <Localized> in ${filename}`,
      )
    }
  }
  return messageId
}

function makeImportDeclarations(this: Context, dependencies: Dependencies) {
  return [
    makeImportDeclaration(
      LOCALIZATION_CONTEXT_MODULE,
      [],
      LOCALIZATION_CONTEXT_COMPONENT_NAME,
    ),
    ...Object.values(dependencies)
      .map(dependency =>
        Object.values(dependency).map(({ name, path }) =>
          makeImportDeclaration(path, [], name),
        ),
      )
      .flat(),
  ]
}

function getLocales(localeDir: string): string[] {
  return readdirSync(localeDir).filter(dir =>
    statSync(join(localeDir, dir)).isDirectory(),
  )
}
