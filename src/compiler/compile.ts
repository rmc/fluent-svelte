import {
  Pattern,
  Expression,
  ComplexPattern,
  MessageReference,
  TermReference,
  FunctionReference,
  SelectExpression,
  NamedArgument,
  Variant,
  VariableReference,
  Literal,
  StringLiteral,
  NumberLiteral,
  Message,
  Term,
  PatternElement,
} from '@fluent/bundle/esm/ast'
import {
  Expression as ExpressionNode,
  ImportDeclaration,
  ConditionalExpression,
  ObjectExpression,
} from 'estree'
import {
  Ast,
  TemplateNode,
  IfBlock,
  InstanceNode,
  makeAst,
  makeText,
  makeIdentifier,
  makeArrayExpression,
  makeCallExpression,
  makeObjectExpression,
  makeNumberLiteral,
  makeStringLiteral,
  makeProperty,
  makeMustacheTag,
  makeElseBlock,
  makeIfBlock,
  makeAttribute,
  makeMemberExpression,
  makeSlot,
  makeProgram,
  Program,
  makeFunctionDeclaration,
  makeExportDefaultDeclaration,
  makeBlockStatement,
  makeBinaryExpression,
  makeNewExpression,
  makeConditionalExpression,
  makeImportDeclaration,
  makeReactiveExpression,
  makePropDeclaration,
} from './types'
import { camelCase } from './shared'
import { FluentResource } from '@fluent/bundle'

type Dependency = {
  path: string
  namedImports?: string[]
  defaultImport?: string
}

type ResultType = 'template' | 'expression'

interface ResultBase {
  type: ResultType
  inputs: string[] // message args are compiled to inputs
  deps: Dependency[]
}

interface TemplateResult extends ResultBase {
  type: 'template'
  value: TemplateNode[]
}

interface ExpressionResult extends ResultBase {
  type: 'expression'
  value: ExpressionNode
}

type Result = TemplateResult | ExpressionResult

type Arguments = {
  positional: ExpressionNode
  named: ExpressionNode
  inputs: string[]
  deps: Dependency[]
}

interface OverlayExpression {
  type: 'overlay'
  name: string
  value?: Pattern // Nested overlays not supported
}

export type CompilationTarget = 'svelte' | 'js' | 'auto'

export type CompilationResultSvelte = {
  type: 'svelte'
  ast: Ast
}

export type CompilationResultJs = {
  type: 'js'
  program: Program
}

export type CompilationResult = CompilationResultSvelte | CompilationResultJs

const DEFAULT_RUNTIME_MODULE = 'fluent-svelte/runtime'
const DEFAULT_SLOT_ATTRS_BINDING = 'attrs'
const DEFAULT_CONTEXT_PROP_NAME = 'context'

const openingTagRegexp = /<([A-Z]\w+?)>/
const closingTagRegexp = /<\/([A-Z]\w+?)>/
const selfClosingTagRegexp = /<([A-Z]\w+?)\s*?\/>/

export class Compiler {
  public messages: Map<string, Message> = new Map()
  private terms: Map<string, Term> = new Map()
  // private useIsolating: boolean
  private runtimeModule: string
  private slotAttrsBinding: string
  private contextPropName: string
  private context?: {
    messageId: string
  }

  constructor({
    // useIsolating = true,
    runtimeModule = DEFAULT_RUNTIME_MODULE,
    slotAttrsBinding = DEFAULT_SLOT_ATTRS_BINDING,
    contextPropName = DEFAULT_CONTEXT_PROP_NAME,
  }: {
    useIsolating?: boolean
    runtimeModule?: string
    slotAttrsBinding?: string
    contextPropName?: string
  } = {}) {
    this.terms = new Map()
    this.messages = new Map()
    this.runtimeModule = runtimeModule
    this.slotAttrsBinding = slotAttrsBinding
    this.contextPropName = contextPropName
    // this.useIsolating = useIsolating
  }

  public addResource(res: FluentResource, { allowOverrides = false } = {}) {
    const errors = []
    for (let i = 0; i < res.body.length; i++) {
      let entry = res.body[i]
      if (entry.id.startsWith('-')) {
        if (allowOverrides === false && this.terms.has(entry.id)) {
          errors.push(
            new Error(
              this.makeLogMessage(
                `Attempt to override an existing term: "${entry.id}"`,
              ),
            ),
          )
          continue
        }
        this.terms.set(entry.id, entry as Term)
      } else {
        if (allowOverrides === false && this.messages.has(entry.id)) {
          errors.push(
            new Error(
              this.makeLogMessage(
                `Attempt to override an existing message: "${entry.id}"`,
              ),
            ),
          )
          continue
        }
        this.messages.set(entry.id, entry as Message)
      }
    }
    return errors
  }

  public compileMessageToSvelte(message: Message) {
    return (this.compileMessage(message, 'svelte') as CompilationResultSvelte)
      .ast
  }

  public compileMessageToModule(message: Message) {
    return (this.compileMessage(message, 'svelte') as CompilationResultJs)
      .program
  }

  public compileMessage(
    { id, value, attributes }: Message,
    target: CompilationTarget,
  ): CompilationResult {
    this.context = { messageId: id }
    const hasAttrs = Object.values(attributes).length
    const hasOverlays = false // TODO

    const resultType =
      (target === 'auto' && (hasAttrs || hasOverlays)) || target === 'svelte'
        ? 'template'
        : 'expression'
    const messageResult = this.compileMessagePattern(value, resultType)

    if (messageResult.type === 'expression') {
      return this.compileModule(messageResult)
    }

    const html = (messageResult as TemplateResult).value
    const inputs = messageResult.inputs
    const deps = messageResult.deps

    if (hasAttrs) {
      const attributesResult = this.compileAttributes(attributes)
      inputs.push(...attributesResult.inputs)
      deps.push(...attributesResult.deps)
      html.push(...attributesResult.value)
    }

    let instance
    if (inputs.length || deps.length) {
      instance = this.compileInstance(inputs, deps)
    }
    return {
      type: 'svelte',
      ast: makeAst(html, instance),
    }
  }

  private compileAttributes(
    attributes: Record<string, Pattern>,
  ): TemplateResult {
    const inputs = []
    const deps = []
    const attrsObj: ObjectExpression = makeObjectExpression()
    for (const [name, value] of Object.entries(attributes)) {
      const result = this.compileMessagePattern(
        value,
        'expression',
      ) as ExpressionResult
      inputs.push(...result.inputs)
      deps.push(...result.deps)
      attrsObj.properties.push(
        makeProperty(makeStringLiteral(name), result.value),
      )
    }
    return this.makeTemplateResult(
      makeSlot([
        makeAttribute(this.slotAttrsBinding, [makeMustacheTag(attrsObj)]),
      ]),
      inputs,
      deps,
    )
  }

  private compileMessagePattern(
    pattern: Pattern | null,
    resultType: ResultType,
  ): Result {
    if (resultType === 'expression') {
      if (typeof pattern === 'string') {
        return this.makeExpressionResult(makeStringLiteral(pattern))
      } else if (pattern === null) {
        return this.makeExpressionResult(makeStringLiteral(''))
      } else {
        return this.compileComplexPatternToExpression(pattern)
      }
    }
    if (pattern === null) {
      return this.makeTemplateResult(makeText(''))
    }
    const extendedPattern = this.extendPatternWithOverlays(pattern)
    if (typeof extendedPattern === 'string') {
      return this.makeTemplateResult(makeText(extendedPattern))
    }
    return this.compileComplexPatternToTemplate(
      extendedPattern as ComplexPattern,
    )
  }

  private compileComplexPatternToTemplate(
    pattern: ComplexPattern,
  ): TemplateResult {
    const value = []
    const inputs = []
    const deps = []
    for (const el of pattern) {
      let result
      if (typeof el === 'string') {
        result = this.makeTemplateResult(makeText(el))
      } else {
        result = this.compileExpression(el, 'template') as TemplateResult
      }
      value.push(...result.value)
      inputs.push(...result.inputs)
      deps.push(...result.deps)
    }
    return this.makeTemplateResult(value, inputs, deps)
  }

  private compileComplexPatternToExpression(
    pattern: ComplexPattern,
  ): ExpressionResult {
    let expression: ExpressionNode | null = null
    const inputs = []
    const deps = []
    for (const el of pattern) {
      let result: ExpressionResult
      if (typeof el === 'string') {
        result = this.compileExpression(
          { type: 'str', value: el },
          'expression',
        ) as ExpressionResult
      } else if (el.type === 'num') {
        result = this.compileNumToStringExpression(el.value)
      } else if (el.type === 'var' || el.type === 'func') {
        const ir = this.compileExpression(el, 'expression') as ExpressionResult
        result = this.makeExpressionResult(
          makeCallExpression(
            makeMemberExpression(ir.value, makeIdentifier('toString'), false),
          ),
          ir.inputs,
          ir.deps,
        )
      } else {
        result = this.compileExpression(el, 'expression') as ExpressionResult
      }
      if (expression === null) {
        expression = result.value
      } else {
        expression = makeBinaryExpression('+', expression, result.value)
      }
      inputs.push(...result.inputs)
      deps.push(...result.deps)
    }
    return this.makeExpressionResult(expression as ExpressionNode, inputs, deps)
  }

  private compileExpression(
    expr: Expression | OverlayExpression,
    resultType: ResultType,
  ): Result {
    switch (expr.type) {
      case 'str':
        return this.compileStrExpression(expr, resultType)
      case 'num':
        return this.compileNumExpression(expr, resultType)
      case 'var':
        return this.compileVariableReference(expr, resultType)
      case 'mesg':
        return this.compileMessageReference(expr, resultType)
      case 'term':
        return this.compileTermReference(expr, resultType)
      case 'func':
        return this.compileFuncReference(expr, resultType)
      case 'select':
        return this.compileSelectExpression(expr, resultType)
      case 'overlay':
        return this.compileOverlayExpression(expr)
      default:
        throw new Error(
          this.makeLogMessage(`Unknown expression\n${JSON.stringify(expr)}`),
        )
    }
  }

  private compileOverlayExpression({
    name,
    value,
  }: OverlayExpression): TemplateResult {
    const attrs = [makeAttribute('name', [makeText(name)])]
    if (value) {
      const result = this.compileMessagePattern(
        value,
        'expression',
      ) as ExpressionResult
      attrs.push(makeAttribute('text', [makeMustacheTag(result.value)]))
      return this.makeTemplateResult(
        makeSlot(attrs),
        result.inputs,
        result.deps,
      )
    }
    return this.makeTemplateResult(makeSlot(attrs))
  }

  private compileSelectExpression(
    { selector, star, variants }: SelectExpression,
    resultType: ResultType,
  ): Result {
    if (variants.length === 1) {
      return this.compileMessagePattern(variants[0].value, resultType)
    }

    const selectorResult = this.compileExpression(
      selector,
      'expression',
    ) as ExpressionResult
    if (!selectorResult) {
      throw new Error(this.makeLogMessage('Invalid selector'))
    }

    // Move stared variant to last position
    variants.sort((a, b) => {
      const aIndex = variants.indexOf(a)
      if (aIndex === star) {
        return 1
      }
      const bIndex = variants.indexOf(b)
      if (bIndex === star) {
        return -1
      }

      return aIndex - bIndex
    })

    const inputs: string[] = [this.contextPropName, ...selectorResult.inputs]
    const deps: Dependency[] = [
      ...selectorResult.deps,
      { path: this.runtimeModule, namedImports: ['match'] },
    ]

    switch (resultType) {
      case 'expression':
        const expressionResult = this.compileSelectToExpression(
          selectorResult.value,
          variants,
        )
        return this.makeExpressionResult(
          expressionResult.value,
          [...inputs, ...expressionResult.inputs],
          [...deps, ...expressionResult.deps],
        )
      case 'template':
        const templateResult = this.compileSelectToTemplate(
          selectorResult.value,
          variants,
        )
        return this.makeTemplateResult(
          templateResult.value,
          [...inputs, ...templateResult.inputs],
          [...deps, ...templateResult.deps],
        )
    }
  }

  private compileSelectToExpression(
    selector: ExpressionNode,
    variants: Variant[],
  ): ExpressionResult {
    const inputs = []
    const deps = []
    let expression: ConditionalExpression | null = null

    for (let i = 0; i < variants.length - 1; i++) {
      const variant = variants[i]
      const variantResult = this.compileMessagePattern(
        variant.value,
        'expression',
      ) as ExpressionResult
      inputs.push(...variantResult.inputs)
      deps.push(...variantResult.deps)

      const test = makeCallExpression(makeIdentifier('match'), [
        makeIdentifier(this.contextPropName),
        selector,
        this.fluentLiteralToExpression(variant.key),
      ])
      const variantExpression = makeConditionalExpression(
        test,
        variantResult.value,
        makeStringLiteral(''),
      )
      if (expression === null) {
        expression = variantExpression
      } else if (i < variants.length - 1) {
        expression.alternate = variantExpression
      } else {
        expression.alternate = variantResult.value
      }
    }
    return this.makeExpressionResult(expression as ExpressionNode, inputs, deps)
  }

  private compileSelectToTemplate(
    selector: ExpressionNode,
    variants: Variant[],
  ): TemplateResult {
    const inputs = []
    const deps = []
    let ifBlock: IfBlock | null = null
    let result: TemplateResult | null = null

    for (let i = 0; i < variants.length; i++) {
      const variant = variants[i]
      const variantResult = this.compileMessagePattern(
        variant.value,
        'template',
      ) as TemplateResult
      inputs.push(...variantResult.inputs)
      deps.push(...variantResult.deps)

      const test = makeCallExpression(makeIdentifier('match'), [
        makeIdentifier(this.contextPropName),
        selector,
        this.fluentLiteralToExpression(variant.key),
      ])
      const variantIfBlock = makeIfBlock(test, variantResult.value)
      if (ifBlock === null) {
        ifBlock = variantIfBlock
        result = this.makeTemplateResult(ifBlock as TemplateNode, inputs, deps)
      } else if (i < variants.length - 1) {
        variantIfBlock.elseif = true
        ifBlock.else = makeElseBlock([variantIfBlock])
        ifBlock = variantIfBlock
      } else {
        ifBlock.else = makeElseBlock(variantResult.value)
      }
    }
    return result as TemplateResult
  }

  private fluentLiteralToExpression(literal: Literal): ExpressionNode {
    switch (literal.type) {
      case 'str':
        return makeStringLiteral(literal.value)
      case 'num':
        return makeNumberLiteral(literal.value)
    }
  }

  private compileFuncReference(
    { name, args }: FunctionReference,
    resultType: ResultType,
  ): Result {
    const { positional, named, ...argsResult } = this.compileFuncArgs(args)
    switch (resultType) {
      case 'expression':
        return this.makeExpressionResult(
          makeCallExpression(makeIdentifier('callFunctionReference'), [
            makeIdentifier(this.contextPropName),
            makeStringLiteral(name),
            positional,
            named,
          ]),
          [this.contextPropName, ...argsResult.inputs],
          [
            ...argsResult.deps,
            {
              path: this.runtimeModule,
              namedImports: ['callFunctionReference'],
            },
          ],
        )
      case 'template':
        return this.makeTemplateResult(
          makeMustacheTag(
            makeCallExpression(
              makeMemberExpression(
                makeCallExpression(makeIdentifier('callFunctionReference'), [
                  makeIdentifier(this.contextPropName),
                  makeStringLiteral(name),
                  positional,
                  named,
                ]),
                makeIdentifier('toString'),
                false,
              ),
            ),
          ),
          [...argsResult.inputs, this.contextPropName],
          [
            ...argsResult.deps,
            {
              path: this.runtimeModule,
              namedImports: ['callFunctionReference'],
            },
          ],
        )
    }
  }

  private compileFuncArgs(args: Array<Expression | NamedArgument>): Arguments {
    const positional: ExpressionNode[] = []
    const named: [ExpressionNode, ExpressionNode][] = []
    const inputs = []
    const deps = []

    for (const arg of args) {
      let result
      if (arg.type === 'narg') {
        result = this.compileExpression(
          arg.value,
          'expression',
        ) as ExpressionResult
        if (result) {
          named.push([makeIdentifier(arg.name), result.value])
        }
      } else {
        result = this.compileExpression(arg, 'expression') as ExpressionResult
        if (result) {
          positional.push(result.value)
        }
      }
      if (result) {
        inputs.push(...result.inputs)
        deps.push(...result.deps)
      }
    }

    return {
      positional: makeArrayExpression(positional),
      named: makeObjectExpression(
        named.map(([key, value]) => makeProperty(key, value)),
      ),
      inputs,
      deps,
    }
  }

  private compileTermReference(
    { name, attr }: TermReference,
    resultType: ResultType,
  ): Result {
    const id = `-${name}`
    const term = this.terms.get(id)
    if (!term) {
      throw new Error(this.makeLogMessage(`Unknown term ${id}`))
    }
    if (attr) {
      const attribute = term.attributes[attr]
      if (!attribute) {
        throw new Error(
          this.makeLogMessage(`Unknown attribute ${attr} in term ${id}`),
        )
      }
      return this.compileMessagePattern(attribute, resultType)
    }
    return this.compileMessagePattern(term.value, resultType)
  }

  private compileMessageReference(
    { name, attr }: MessageReference,
    resultType: ResultType,
  ): Result {
    const message = this.messages.get(name)
    if (!message) {
      throw new Error(this.makeLogMessage(`Unknown message ${name}`))
    }
    if (attr) {
      const attribute = message.attributes[attr]
      if (!attribute) {
        throw new Error(
          this.makeLogMessage(`Unknown attribute ${attr} in message ${name}`),
        )
      }
      return this.compileMessagePattern(attribute, resultType)
    }
    return this.compileMessagePattern(message.value, resultType)
  }

  private compileVariableReference(
    { name }: VariableReference,
    resultType: ResultType,
  ): Result {
    switch (resultType) {
      case 'expression':
        return this.makeExpressionResult(makeIdentifier(name), [
          this.contextPropName,
          name,
        ])
      case 'template':
        return this.makeTemplateResult(
          makeMustacheTag(
            makeCallExpression(
              makeMemberExpression(
                makeIdentifier(name),
                makeIdentifier('toString'),
                false,
              ),
            ),
          ),
          [this.contextPropName, name],
        )
    }
  }

  private compileNumExpression(
    { value }: NumberLiteral,
    resultType: ResultType,
  ): Result {
    switch (resultType) {
      case 'expression':
        return this.makeExpressionResult(makeNumberLiteral(value))
      case 'template':
        const exprResult = this.compileNumToStringExpression(value)
        return this.makeTemplateResult(
          makeMustacheTag(exprResult.value),
          exprResult.inputs,
          exprResult.deps,
        )
    }
  }

  private compileNumToStringExpression(value: number): ExpressionResult {
    return this.makeExpressionResult(
      makeCallExpression(
        makeMemberExpression(
          makeCallExpression(
            makeMemberExpression(
              makeNewExpression(makeIdentifier('FluentNumber'), [
                makeNumberLiteral(value),
              ]),
              makeIdentifier('setContext'),
              false,
            ),
            [makeIdentifier(this.contextPropName)],
          ),
          makeIdentifier('toString'),
          false,
        ),
        [],
      ),
      [],
      [{ path: this.runtimeModule, namedImports: ['FluentNumber'] }],
    )
  }

  private compileStrExpression(
    { value }: StringLiteral,
    resultType: ResultType,
  ): Result {
    switch (resultType) {
      case 'expression':
        return this.makeExpressionResult(makeStringLiteral(value))
      case 'template':
        return this.makeTemplateResult(
          makeMustacheTag(makeStringLiteral(value)),
        )
    }
  }

  private extendPatternWithOverlays(
    pattern: Pattern,
  ): string | (PatternElement | OverlayExpression)[] {
    if (typeof pattern === 'string') {
      return this.extendStrPatternWithOverlays(pattern)
    }
    return this.extendComplexPatternWithOverlays(pattern)
  }

  private extendComplexPatternWithOverlays(
    pattern: ComplexPattern,
  ): string | (PatternElement | OverlayExpression)[] {
    const result = []
    let i = 0
    while (i < pattern.length) {
      let el = pattern[i]
      let text
      if (typeof el === 'string') {
        text = el
      } else if (el.type === 'str') {
        text = el.value
      }
      if (text != null) {
        const openingTagMatch = openingTagRegexp.exec(text)
        const selfClosingTagMatch = selfClosingTagRegexp.exec(text)
        if (selfClosingTagMatch) {
          const [, name] = selfClosingTagMatch
          const overlayExpression: OverlayExpression = {
            type: 'overlay',
            name,
          }
          const preTagText = text.slice(0, selfClosingTagMatch.index)
          const postTagText = text.slice(
            selfClosingTagMatch.index + selfClosingTagMatch[0].length,
          )
          if (preTagText.length) {
            result.push(preTagText)
          }
          result.push(overlayExpression)
          if (postTagText.length) {
            const postTagResult = this.extendPatternWithOverlays(postTagText)
            if (Array.isArray(postTagResult)) {
              result.push(...postTagResult)
            } else {
              result.push(postTagResult)
            }
          }
          i++
        } else if (openingTagMatch) {
          const [, name] = openingTagMatch
          const overlayExpression: OverlayExpression = {
            type: 'overlay',
            name,
          }
          const preTagText = text.slice(0, openingTagMatch.index)
          let postTagText
          const value: Pattern = []
          let closingTagMatch

          closingTagMatch = closingTagRegexp.exec(text)
          if (closingTagMatch && closingTagMatch[1] === name) {
            value.push(
              text.slice(
                openingTagMatch.index + openingTagMatch[0].length,
                closingTagMatch.index,
              ),
            )
            postTagText = text.slice(
              closingTagMatch.index + closingTagMatch[0].length,
            )
          } else {
            if (text.length > openingTagMatch.index + openingTagMatch[0].length)
              value.push(
                text.slice(openingTagMatch.index + openingTagMatch[0].length),
              )
            closingTagMatch = null

            let childEl
            let j = i + 1

            while (!closingTagMatch || j < pattern.length) {
              childEl = pattern[j]
              if (typeof childEl === 'string') {
                text = childEl
              } else if (childEl.type === 'str') {
                text = (childEl as StringLiteral).value
              }
              closingTagMatch = closingTagRegexp.exec(text)
              if (!closingTagMatch || closingTagMatch[1] !== name) {
                closingTagMatch = null
                value.push(childEl)
              } else {
                value.push(text.slice(0, closingTagMatch.index))
              }
              j++
            }
            if (!closingTagMatch) {
              break
            }
            if (typeof childEl === 'string') {
              postTagText = childEl.slice(
                closingTagMatch.index + closingTagMatch[0].length,
              )
            } else {
              postTagText = (childEl as StringLiteral).value.slice(
                closingTagMatch.index + closingTagMatch[0].length,
              )
            }
            i = j

            el = childEl as PatternElement
          }

          overlayExpression.value = value
          if (preTagText.length) {
            result.push(preTagText)
          }
          result.push(overlayExpression)
          if (postTagText.length) {
            const postTagResult = this.extendPatternWithOverlays(postTagText)
            if (Array.isArray(postTagResult)) {
              result.push(...postTagResult)
            } else {
              result.push(postTagResult)
            }
          }
          i++
        } else {
          result.push(el)
          i++
        }
      } else {
        result.push(el)
        i++
      }
    }
    return result
  }

  private extendStrPatternWithOverlays(
    pattern: string,
  ): string | (PatternElement | OverlayExpression)[] {
    const result: (PatternElement | OverlayExpression)[] = []
    const selfClosingTagMatch = selfClosingTagRegexp.exec(pattern)

    if (selfClosingTagMatch) {
      const [, name] = selfClosingTagMatch
      const preTagText = pattern.slice(0, selfClosingTagMatch.index)
      const postTagText = pattern.slice(
        selfClosingTagMatch.index + selfClosingTagMatch[0].length,
      )
      if (preTagText.length) {
        result.push({ type: 'str', value: preTagText })
      }
      result.push({ type: 'overlay', name, value: '' })
      if (postTagText.length) {
        result.push({ type: 'str', value: postTagText })
      }
      return result
    }

    const openingTagMatch = openingTagRegexp.exec(pattern)
    const closingTagMatch = closingTagRegexp.exec(pattern)
    if (
      openingTagMatch &&
      closingTagMatch &&
      openingTagMatch[1] === closingTagMatch[1]
    ) {
      const [, name] = openingTagMatch
      const preTagText = pattern.slice(0, openingTagMatch.index)
      const postTagText = pattern.slice(
        closingTagMatch.index + closingTagMatch[0].length,
      )
      const childText = pattern.slice(
        openingTagMatch.index + openingTagMatch[0].length,
        closingTagMatch.index,
      )
      if (preTagText.length) {
        result.push({ type: 'str', value: preTagText })
      }
      result.push({ type: 'overlay', name, value: childText })
      if (postTagText.length) {
        result.push({ type: 'str', value: postTagText })
      }
      return result
    }
    return pattern
  }

  private compileModule(result: ExpressionResult): CompilationResultJs {
    const params = [makeIdentifier(this.contextPropName)]
    if (result.inputs) {
      params.push(
        ...Array.from(new Set(...result.inputs).values()).map(makeIdentifier),
      )
    }
    return {
      type: 'js',
      program: makeProgram(
        [
          ...this.depsToImports(result.deps),
          makeExportDefaultDeclaration(
            makeFunctionDeclaration(
              makeIdentifier(camelCase(this.context!.messageId)),
              [makeIdentifier(this.contextPropName)],
              makeBlockStatement([]), // TODO
            ),
          ),
        ],
        'module',
      ),
    }
  }

  private compileInstance(
    inputs: string[],
    deps: Dependency[],
  ): InstanceNode[] {
    inputs = Array.from(new Set(inputs).values())
    if (inputs.length) {
      deps.push({
        path: this.runtimeModule,
        namedImports: ['resolveVariableReference'],
      })
    }
    const result: InstanceNode[] = this.depsToImports(deps)
    for (const name of inputs) {
      result.push(makePropDeclaration(makeIdentifier(name)))
      if (name !== this.contextPropName) {
        result.push(
          makeReactiveExpression(
            makeIdentifier(name),
            makeCallExpression(makeIdentifier('resolveVariableReference'), [
              makeIdentifier(this.contextPropName),
              makeStringLiteral(name),
              makeIdentifier(name),
            ]),
          ),
        )
      }
    }
    return result
  }

  private depsToImports(deps: Dependency[]): ImportDeclaration[] {
    return Array.from(
      deps
        .reduce((acc, d) => {
          const dep = acc.get(d.path)
          if (dep) {
            acc.set(dep.path, {
              ...dep,
              namedImports: Array.from(
                new Set([
                  ...dep.namedImports,
                  ...(d.namedImports || []),
                ]).values(),
              ),
            })
          } else {
            acc.set(d.path, d)
          }
          return acc
        }, new Map())
        .values(),
    ).map(d => makeImportDeclaration(d.path, d.namedImports, d.defaultImport))
  }

  private makeTemplateResult(
    value: TemplateNode | TemplateNode[],
    inputs: string[] = [],
    deps: Dependency[] = [],
  ): TemplateResult {
    return {
      type: 'template',
      value: Array.isArray(value) ? value : [value],
      inputs,
      deps,
    }
  }

  private makeExpressionResult(
    value: ExpressionNode,
    inputs: string[] = [],
    deps: Dependency[] = [],
  ): ExpressionResult {
    return {
      type: 'expression',
      value,
      inputs,
      deps,
    }
  }

  private makeLogMessage(value: string) {
    return `fluent-svelte compiler: ${value}`
  }
}
