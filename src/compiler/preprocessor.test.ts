import { preprocessor } from '.'
import { Processed } from 'svelte/types/compiler/preprocess'

const preprocess = preprocessor({
  locales: ['en', 'pt-PT'],
  dir: 'src/locales',
})

function getProcessed(content: string) {
  return (preprocess.markup!({
    content,
    filename: 'test',
  }) as Processed).code
}

it('should process correctly with script', () => {
  expect(
    getProcessed(`
  <script context="module">
    export const hello = 'world';
  </script>

  <script>
    import { FluentNumber } from "fluent-svelte/runtime";
  </script>

  <style>
    button {
      color: red;
    }
  </style>

  <Localized id="amount-owed" amount={new FluentNumber(24.99, { currency: 'EUR' })} />
  `),
  ).toMatchSnapshot()
})

it('should process correctly without script', () => {
  expect(
    getProcessed(`
  <script context="module">
    export const hello = 'world';
  </script>

  <style>
    button {
      color: red;
    }
  </style>

  <Localized id="hello" name="world" />
  `),
  ).toMatchSnapshot()
})

it('should wrap multiple <Localized> with single <LocalizationContext>', () => {
  expect(
    getProcessed(`
  <Span>sda</Span>

  <Localized id="hello" name="world" />

  <div>
    <Localized id="amount-owed" amount={new FluentNumber(24.99, { currency: 'EUR' })} />
  </div>

  <InBetween>asda
    <Localized id="hello" name="world" />
  </InBetween>

  <Localized id="hello" name="world" />

  <Foo>asda</Foo>
  `),
  ).toMatchSnapshot()
})

it('should wrap <Localized> inside IfBlocks with single <LocalizationContext>', () => {
  expect(
    getProcessed(`
  {#if num === 0}
    <Localized id="items-select" />
  {:else}
    <Localized id="items-selected" {num} />
  {/if}
  `),
  ).toMatchSnapshot()
})

it('should throw when id is missing', () => {
  expect(() =>
    getProcessed(`
    <Localized />
  `),
  ).toThrowErrorMatchingSnapshot()
})

it('should throw when id is empty', () => {
  expect(() =>
    getProcessed(`
    <Localized id=""/>
  `),
  ).toThrowErrorMatchingSnapshot()
})

it('should throw when id is computed', () => {
  expect(() =>
    getProcessed(`
    <Localized id={foo}/>
  `),
  ).toThrowErrorMatchingSnapshot()
})
