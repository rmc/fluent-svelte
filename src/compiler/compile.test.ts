import { FluentResource } from '@fluent/bundle'
import { Compiler, print } from '.'
import { Message } from '@fluent/bundle/esm/ast'

const messages = `
items-selected =
  { $num ->
      [one] One item selected.
     *[other] { $num } items selected.
  }

emails =
  { $unreadEmails ->
      [one] You have one unread email.
     *[other] You have { $unreadEmails } unread emails.
  }

your-score =
  { NUMBER($score, minimumFractionDigits: 1) ->
      [0.0]   You scored zero points. What happened?
     *[other] You scored { NUMBER($score, minimumFractionDigits: 1) } points.
  }

your-rank = { NUMBER($pos, type: "ordinal") ->
   [1] You finished first!
   [one] You finished {$pos}st
   [two] You finished {$pos}nd
   [few] You finished {$pos}rd
  *[other] You finished {$pos}th
}

amount-owed = You owe { NUMBER($amount, currencyDisplay: "code", useGrouping: "false") }

info = Read the <Link>documentation</Link> for more information.

confirm = Do you want to delete all your emails?
  .ok     = Yes
  .cancel = No

submit-button = <Button>Pay</Button>
  .aria-label = Proceed to payment

items-select = Select items

download-block =
  You can download { $brandName } by clicking
  on the <span>Download</span> button or read
  the <Link>release notes</Link> to learn more.

intro-block =
  Our values are the best and we only care about
  doing good <Img /> for the world.

overlay-with-placeable = <Slotname>before text { $placeable } after text</Slotname>

menu-save = Save
help-menu-save = Click { menu-save } to save the file.

-brand-name = Firefox
installing = Installing { -brand-name }.

opening-brace = This message features an opening curly brace: {"{"}.
closing-brace = This message features a closing curly brace: {"}"}.

blank-is-removed =     This message starts with no blanks.
blank-is-preserved = {"    "}This message starts with 4 spaces.
overlay-with-blank = <Link>{"    "}This message starts with 4 spaces.</Link>

literal-quote1 = Text in {"\""}double quotes{"\""}.
literal-quote2 = Text in "double quotes".

privacy-label = Privacy{"\\u00A0"}Policy
which-dash2 = It's a dash{"\\u2014"}or is it?

tears-of-joy1 = {"\\U01F602"}
tears-of-joy2 = 😂
`

const compiler = new Compiler()
const parsingErrors = compiler.addResource(new FluentResource(messages))

beforeAll(() => {
  expect(parsingErrors.length).toBe(0)
})

it.each(
  Array.from(compiler.messages.values()).map(message => [
    message.id,
    message,
  ]) as [string, Message][],
)(`compiles the message %s correctly`, (_, message: Message) => {
  const ast = compiler.compileMessageToSvelte(message)
  expect(print(ast)).toMatchSnapshot()
})
