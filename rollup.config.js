import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import typescript from '@rollup/plugin-typescript'
import executable from 'rollup-plugin-executable'

const external = id =>
  id.startsWith('fluent-svelte/') ||
  id.startsWith('svelte') ||
  id.startsWith('prettier') ||
  id.startsWith('recast')

const paths = id =>
  id.startsWith('fluent-svelte/') && `${id.replace('fluent-svelte', '.')}`

export default [
  {
    input: 'src/index.ts',
    plugins: [
      resolve(),
      commonjs(),
      typescript({
        typescript: require('typescript'),
      }),
    ],
    external,
    output: [
      { file: 'index.js', format: 'umd', name: 'fluent-svelte', paths },
      { file: 'index.mjs', format: 'esm', name: 'fluent-svelte', paths },
    ],
  },
  {
    input: 'src/compiler/index.ts',
    plugins: [
      resolve(),
      commonjs(),
      typescript({
        typescript: require('typescript'),
      }),
    ],
    external,
    output: [
      { file: 'compiler.js', format: 'umd', name: 'fluent-svelte/compiler' },
    ],
  },
  {
    input: 'src/cli.ts',
    plugins: [
      resolve(),
      commonjs(),
      typescript({
        typescript: require('typescript'),
      }),
      executable(),
    ],
    external,
    output: [
      {
        file: 'cli.js',
        format: 'cjs',
        paths,
        banner: '#!/usr/bin/env node',
      },
    ],
  },
  {
    input: 'src/runtime/index.ts',
    plugins: [
      resolve(),
      commonjs(),
      typescript({
        typescript: require('typescript'),
      }),
    ],
    external,
    output: [
      { file: 'runtime.mjs', format: 'esm' },
      { file: 'runtime.js', format: 'cjs' },
    ],
  },
]
