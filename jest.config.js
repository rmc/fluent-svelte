module.exports = {
  preset: 'ts-jest/presets/js-with-ts',
  testEnvironment: 'node',
  moduleNameMapper: {
    'fluent-svelte/compiler': '<rootDir>/src/compiler',
  },
  transformIgnorePatterns: ['node_modules/(?!(.*))'],
  globals: {
    'ts-jest': {
      isolatedModules: true,
    },
  },
}
