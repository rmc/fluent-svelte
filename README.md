# fluent-svelte

Svelte bindings for Project Fluent

Moves Fluent message formatting to compile time and provides support for embedding elements/components into messages.

## Installation

```bash
npm install -D fluent-svelte
```

```bash
yarn add -D fluent-svelte
```

## Usage

### Use the `fluent-svelte` CLI to compile messages to components

Add a script to the package.json

```json
"scripts": {
    "l10n": "fluent-svelte src/components/locale messages/*",
    "predev": "npm run l10n",
    "prebuild": "npm run l10n",
```

Running the script will generate a Svelte component for each message in a dir named with the locale infered from the message file assuming the message file is named like `messages.[locale].ftl`.

### Use the `fluent-svelte` Svelte preprocessor to automatically import the message components

```js
import fluentSvelte from 'fluent-svelte/preprocessor'

svelte({
  preprocess: fluentSvelte({
    dir: './src/components/locale',
    locales: ['en', 'pt-PT'], // by default gets the locales from the `dir` sub-dirs
    componentName: 'Localized', // override the default component name of Localized
  }),
})
```

### Component usage

Render a `<FluentProvider />` to provide the `FluentContext`

Use `<Localized id="message-id" {...args} />` to render a message.

**Note:** Computed message ids are not supported, render separate `<Localized>` instead.

```html
<script>
  import LocalizationProvider from 'fluent-svelte/Provider.svelte'
  import negotiateLocale from './localeNegotiator'
  import {FluentNumber} from 'fluent-svelte/runtime'

  const locale = negotiateLocale(navigator.locales)
</script>

<LocalizationProvider {locale} options={{
    onError: error => {
      // do something with the error
      // If not provided, errors will be thrown
    },
    functions: {
      // custom functions
      NODE_ENV: () => process.env.NODE_ENV,
    }}}>

  <!--
  emails =
    { $unreadEmails ->
        [one] You have one unread email.
       *[other] You have { $unreadEmails } unread emails.
    }
  -->
  <Localized id="emails" unreadEmails={1}/>

  <!--
  After preprocessing:

  <LocalizationContext let:context>
    <svelte:component this={{en: Emails$en, 'pt-PT', Emails$ptPT}[context.locales[0]]} />
  </LocalizationContext>

  <script>
    import LocalizationContext from 'fluent-svelte/Context.svelte'
    import Emails$en from '/components/src/locale/en/emails.svelte'
    import Emails$ptPT from '/components/src/locale/pt-PT/emails.svelte'
  </script>
  -->

  <!--
  amount-owed = You owe { NUMBER($amount, style: "currency", currencyDisplay: "code", useGrouping: "false") }
  -->
  <Localized id="amount-owed" amount={new FluentNumber(24.99, {currency: 'EUR'})}/>

  <!--
  info = Read the <Link>documentation</Link> for more information.
   -->
  <Localized id="info">
    <a slot="Link" href="/" let:text>{text}</a>
  </Localized>

  <!--
  confirm = Do you want to delete all your emails?
    .ok     = Yes
    .cancel = No
  -->
  <Localized id="confirm" let:attrs>
    <button on:click={deleteMessages}>{attrs.ok}</button>
    <button on:click={closeDialog}>{attrs.cancel}</button>
  </Localized>

  <!--
  submit-button = <Button>Pay</Button>
    .aria-label = Proceed to payment
  -->
  <Localized id="submit-button" unreadEmails={100} let:attrs>
    <button slot="Button" aria-label={attrs['aria-label']} let:text type="submit">{text}</button>
  </Localized>

  <!--
  items-select = Select items
  items-selected =
    { $num ->
        [one] One item selected.
       *[other] { $num } items selected.
    }
  -->
  {#if num === 0}
    <Localized id="items-select" />
  {:else}
    <Localized id="items-selected" {num} />
  {/if}


  <!--
  download-block =
    You can download { $brandName } by clicking
    on the <span>Download</span> button or read
    the <Link>release notes</Link> to learn more.
  -->
  <Localized id="download-block" {brandName}>
    <a slot="Link" href="/releases" let:text>{text}</a>
  </Localized>

</LocalizationProvider>
```

## Learn more

Find out more about Project Fluent at [projectfluent.org](projectfluent.org), including documentation of the Fluent file format (FTL).

## TODO

- Compile Fluent message into Svelte ast
  - [x] fluent pattern to Svelte ast
  - [ ] add bidi support
  - [ ] protect against Billion Laughs and Quadratic Blowup attacks (not so severe since it will only happen at compile time but would be nice to have if compiler is embedded into web)
- Print component
  - [x] Svelte ast to Svelte template
- Attributes support
  - [x] expose formatted attributes through default slot let bindings as strings
- Overlay support
  - [x] Detect `<` in message pattern
  - [x] Extract slot name from tag name
    - [x] only extract tag names that are not standard html (start with uppercase)
    - [x] user gets slot children as slot prop `text` ?
    - [ ] ~~user passes overlay props as object and overlay is fully rendered by generated component spreading out the props?~~
- Preprocessor
  - [x] Convert `<Localized id="message-id" {...messageArgs}>` into message component
  - [x] Add imports of message components
  - [x] Wrap all `<Localized>` with single `<FluentContext>`
- [ ] Async Localized: preprocess detects and downloads components that aren't default lang with dynamic import thus code splitting the components into async chunks. this would have to be configured in rollup/webpack (webpack might need magic comments)
- CLI
  - [x] .ftl -> .svelte
  - [ ] improve ux
- Tests
- License
- Changelogs
- Contribution guidelines
- CI

## Nice-to-have ideas or not

- add config file since options are the same for cli and preprocess (and possibly runtime store?)
- runtime store that would take a subscribable of current locales to instantiate the context (and the context key could be private)
- rollup-plugin (to replace CLI)
- webpack-plugin (to replace CLI)
- watch mode for CLI?
- message extraction from svelte template?
  - how would terms be handled?
  - defeats the id only approach
